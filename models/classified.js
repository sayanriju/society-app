var mongoose = require('mongoose');
var moment = require('moment');

var config = require('../config/main')[process.env.NODE_ENV || 'development'];
var awsEndpoint = config.aws.s3Endpoint + '/';
var awsBucket = config.aws.imageBucketName;

var ClassifiedSchema = new mongoose.Schema({
    isOffering: {
        type: Boolean,
        required: true
    },
    for: {
        type: String
    },
    category: {
        type: String
    },
    title: {
        type: String
    },
    price: {
        type: Number
    },
    postAs: {
        type: String,
    },
    contactEmail: {
        type: String
    },
    contactPhone: {
        type: String
    },
    from: {
        type: Date,
        default: Date.now
    },
    validTill: {
        type: Date
    },
    stopDate: { // when to stop showing the ad, specified by an admin
      type: Date,
      default: moment().add(1, 'year').toDate() // if not mentioned, set it to 1 year later by default
    },
    pictureURLs: [String],
    description: {
        type: String
    },
    approvalStatus: {
      type: String,
      default: "Pending",
      enum: ["Approved", "Rejected", "Pending"]
    },
    societyId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Society',
        required: true
    },

    addedBy: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User',
        require: true
    }
});

ClassifiedSchema.virtual('awsPictures').get(function () {
  var base = 'https://'+awsEndpoint+awsBucket+'/' + this._id;
  return [base+'_1', base+'_2', base+'_3'];
});
// ClassifiedSchema.virtual('hasStopped').get(function () { // has reached the stop date specified by Admin (DIFFERENT from expiry/validTill dates)
//   var today = (new Date()).getTime();
//   return (this.stopDate !== undefined && this.stopDate !== null && typeof this.stopDate.getTime === 'function') ? ( today > this.stopDate.getTime()) : false; // if no stopDate date specified, it never expires!
// });
ClassifiedSchema.virtual('hasExpired').get(function () {
  var today = (new Date()).getTime();
  // return (this.validTill !== undefined && this.validTill !== null && typeof this.validTill.getTime === 'function') ? ( today > this.validTill.getTime()) : false; // if no validTill date specified, it never expires!
  var pastValidity = (this.validTill !== undefined && this.validTill !== null && typeof this.validTill.getTime === 'function') ? ( today > this.validTill.getTime()) : false; // if no validTill date specified, it never expires!
  var pastStopDate = (this.stopDate !== undefined && this.stopDate !== null && typeof this.stopDate.getTime === 'function') ? ( today > this.stopDate.getTime()) : false; // if no stopDate date specified, it never expires!

  return (pastValidity || pastStopDate);
});
ClassifiedSchema.virtual('hasStarted').get(function () {
  var today = (new Date()).getTime();
  if (this.from === undefined || this.from === null || typeof this.from.getTime !== 'function') { return true; } // if no start date specified, it is always on!
  return (today >= this.from.getTime());
});

ClassifiedSchema.set('toJSON', {virtuals: true});
ClassifiedSchema.set('toObject', {virtuals: true});

module.exports = mongoose.model("Classified", ClassifiedSchema);
