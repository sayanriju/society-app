var express = require('express');
var router = express.Router();
var  config  =  require('../../../config/main')[process.env.NODE_ENV || 'development'];
var moment = require('moment');

var User = require('../../../models/user');
var Society = require('../../../models/society');

var scheduler = require("./scheduler");

/** start Login Logic **/

// The middleware
function requireLogin(req, res, next) {
  if (!req.session.authenticated || req.session.role != 'Super Admin') {
    res.redirect('/superadmin/logout');
  } else {
    // "globally" set the username & user profile pic for the view rendered in next()
    res.locals.username = req.session.user.name;
    res.locals.profilePicUrl = req.session.user.profilePicUrl;
    next();
  }
};

// The login routes
router.post('/login', function(req, res) {
  if (!req.body.contactEmail || !req.body.password) {
    return res.json({
      error: true,
      message: 'Please provide email and password'
    });
  } else {
    User.findOne({
      contactEmail: req.body.contactEmail,
      role: 'Super Admin'
    }, function(err, user) {
      if (err)
        throw err;
      if (!user) {
        return res.json({
          error: true,
          message: 'User not found'
        });
      } else {
        // password checking
        user.comparePassword(req.body.password, function(err, isMatch) {
          if (isMatch && !err) {
            var data = {
              id: user._id,
              name: user.name,
              role: user.role,
              contactEmail: user.contactEmail,
              profilePicUrl: user.awsProfilePicURL
            };
            req.session.authenticated = true;
            req.session.role = user.role;
            req.session.user = data;
            res.json({
              error: false
            });
          } else {
            return res.json({
              error: true,
              message: "Password did not matched"
            });
          }
        });
      }
    });
  }
});

/* GET login page super admin */
router.get('/login', function(req, res, next) {
  res.render('superadmin/login', {
    title: 'Super Admin Login',
    error: false
  });
});

router.all('/logout', function(req, res) {
  req.session.authenticated = false;
  req.session.user = null;
  req.session.role = null;
  res.redirect('/superadmin/login');
});

/** end Login Logic **/

router.get('/*', requireLogin); // important

router.get('/', function(req, res) {
  // scheduler.showJobs();
  Society.find({}, function(err, societies) {
    if (err) {
      console.log(err);
      return res.render('superadmin/societies', {
        error: true,
        message: "Nothing found"
      });
    } else {
      var societyMap = societies.map(function(society) {
        var rObj = {};
        rObj.id = society._id;
        rObj.societyName = society.societyName;
        rObj.societyAddress = society.societyAddress;
        rObj.contactPerson = society.contactPerson;
        rObj.contactEmail = society.contactEmail;
        rObj.contactPhone = society.contactPhone;
        rObj.joinDate = moment(society.joinDate).format('MMM-YYYY');
        rObj.status = society.status;
        return rObj;

      });
      console.log(societyMap);
      res.render('superadmin/societies', {
        error: false,
        data: societyMap
      });
    }
  });
});


/* add society*/

router.get('/add', function(req, res) {
  res.render('superadmin/add');
});

router.post('/add', function(req, res) {
  var adminData = req.body.admin;
  delete req.body.admin;

  var societyData = req.body;

  if ( !moment(societyData.expiryDate, "DD/MM/YYYY").isValid() || moment(societyData.expiryDate, "DD/MM/YYYY").diff(moment(), 'month') < 1 ) {
    societyData.expiryDate =  moment().add(1, 'month').toDate();
  }

  var society = new Society(societyData);

  society.save(function(err) {
    if (err) {
      console.log(err);
      return res.json({
        error: true,
        message: 'Fill all the details'
      });
    }

    adminData.societyId = society._id;
    adminData.role = 'Admin';
    var user = new User(adminData);

    user.save(function(err) {
      if (err) {
        console.log(err);
        return res.json({
          error: true,
          message: `Specified Admin Email '${adminData.contactEmail}' already exists in our system! Please choose another.`,
          reason: err
        });
      }
      // admin added; let's send an email to him
      var locals = {
        to: adminData.contactEmail,
        // to: 's26c.sayan@gmail.com', // for testing
        subject: 'Welcome to Smart Society!',
        email: adminData.contactEmail,
        password: adminData.password,
        name: adminData.name,
        societyName: societyData.societyName
      }
      console.log(locals);
      res.mailer.send('emails/admin', locals, function (err) {
        if (!err)
        console.log('admin email sent successfully to %s !', adminData.contactEmail);
        //  res.send('done!');
        else {
          console.log("error sending admin email : %o", err);
          // res.send({error: true})
        }
      });
      // don't wait for email sending to complete

      Society.findById(society._id, function(err, soc) {
        if (!err) {
          soc.admin = user._id;
          soc.save(function(err) {
            if (!err) {
              res.json({
                error: false,
                data: {
                  societyId: society._id,
                  adminId: user._id
                }
              });
              // Set up contract expiry reminders
              console.log('***********************************************');
              console.log(society._id, adminData, societyData.expiryDate);
              scheduler.contractExpiryMails(society._id, adminData, societyData.expiryDate);
            } else {
              return res.json({error: true, message: "Something went wrong!"})
            }
          });
        }
      });
    });
  });
});


/* view a society */

router.get('/society/:id', function(req, res) {
  var id = req.params.id;
  Society.findById(id)
    .populate('admin')
    .exec(function(err, society) {
      if (err) {
        return res.render('superadmin/details', {
          error: true,
          data: 'No society found with this id'
        });
      } else {
        society.countReaminingPax(function (err, cnt) {
          return res.render('superadmin/details', {
            error: false,
            data: society,
            remainingPAX: cnt,
            id: id,
            moment: moment
          });
        })
      }
    });
});


router.get('/society/edit/:id', function(req, res) {
  var id = req.params.id;
  Society.findById(id).populate('admin').exec(function(err, society) {
    if (err) {
      return res.render('superadmin/edit', {
        error: true,
        message: 'No society found with this id'
      });
    } else {
      res.render('superadmin/edit', {
        error: false,
        data: society,
        id: id,
        moment: moment
      });
    }
  });
});


router.post('/edit', function(req, res) {
  var adminData = req.body.admin;
  delete req.body.admin;

  var societyData = req.body;

  if ( !moment(societyData.expiryDate, "DD/MM/YYYY").isValid() ) {
    return res.json({
      error: true,
      message: "Invalid format for Expiry Date!"
    })
  } else {
    societyData.expiryDate =  moment(societyData.expiryDate, "DD/MM/YYYY").toDate();
  }

  Society.findByIdAndUpdate(societyData._id, societyData, {
    new: true
  }, function(err, society) {
    if (!err) {
      // If society is being deactivated, deactivate all admins & residents of the society (prevent them from logging in)
      if (societyData.status === 'inactive') {
        User.update({societyId: societyData._id}, { $set: { societyInactive: true }}, {multi: true}).exec();  // don't wait!!
      } else if (societyData.status === 'active') { // If society is being (re)activated, (re)activate all admins & residents of the society (allow them to login)
        User.update({societyId: societyData._id}, { $set: { societyInactive: false }}, {multi: true}).exec(); // don't wait!!
      }

      // (Re)Set up contract expiry reminders
      scheduler.contractExpiryMails(societyData._id, adminData, societyData.expiryDate);

      // Update admin details
      User.findByIdAndUpdate(adminData._id, adminData, {
        new: true
      }, function(err, admin) {
        if (!err) {
          res.json({
            error: false,
            societyId: society._id
          });
        } else {
          res.json({
            error: true,
            message: `Specified Admin Email '${adminData.contactEmail}' already exists in our system! Please choose another.`
          });
        }
      });
    } else {
      res.json({
        error: true,
        message: "Something went wrong!"
      });
    }
  })
});

router.put("/changeadminpass/:adminid", function (req, res) {
  var newPass = req.body.newPass; // plaintext
  User
  .findOne({_id: req.params.adminid, role: "Admin" })
  .exec()
  .then(function (admin) {
    admin.password = newPass;
    return admin.save();
  })
  .then(function (savedAdmin) {
    return res.json({error: false, message: "Password Updated!"});
  })
  .catch(function (err) {
    return res.json({error: true, reason: err, message: "Failed to Update Password!"});
  })
})

module.exports = router;
