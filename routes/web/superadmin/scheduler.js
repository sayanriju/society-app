// var schedule = require('node-schedule');
var Agenda = require("agenda");
var moment = require('moment');
var nodemailer = require('nodemailer');
var smtpTransport = require('nodemailer-smtp-transport');

var config = require('../../../config/main')[process.env.NODE_ENV || 'development'];

var smtpTransport = nodemailer.createTransport({
  transportMethod: 'SMTP',
  host: config.email.host, // host for secure SMTP
  port: config.email.port, // port for secure SMTP
  secure: true, // use SSL
  auth: {
    user: config.email.auth.user,
    pass: config.email.auth.pass
  }
});


module.exports = {
  // contractExpiryMails: function (id, email, expiryDate, updateMode=false) {
  //   var mailer = function () {
  //     console.log('remidner mails to %s !!!!!!!!!!!', email);
  //   }
  //   if (updateMode) { // remove old schedules first
  //     schedule.scheduledJobs['contract-expiry-mail-' + String(id) + '-reminder1'].cancel();
  //     schedule.scheduledJobs['contract-expiry-mail-' + String(id) + '-reminder2'].cancel();
  //     schedule.scheduledJobs['contract-expiry-mail-' + String(id) + '-reminder3'].cancel();
  //     schedule.scheduledJobs['contract-expiry-mail-' + String(id) + '-reminder4'].cancel();
  //   }
  //   // Add new schedules
  //   var date1 = moment(expiryDate).add(-2, 'week').toDate();
  //   var date2 = moment(expiryDate).add(-1, 'week').toDate();
  //   var date3 = moment(expiryDate).add(-2, 'day').toDate();
  //   var date4 = moment(expiryDate).add(-1, 'day').toDate();
  //
  //   var job1 = schedule.scheduleJob('contract-expiry-mail-' + String(id) + '-reminder1', date1, mailer);
  //   var job2 = schedule.scheduleJob('contract-expiry-mail-' + String(id) + '-reminder2', date2, mailer);
  //   var job3 = schedule.scheduleJob('contract-expiry-mail-' + String(id) + '-reminder3', date3, mailer);
  //   var job4 = schedule.scheduleJob('contract-expiry-mail-' + String(id) + '-reminder4', date4, mailer);
  // },
  // showJobs: function () {
  //   console.log(schedule.scheduledJobs);
  // }
  contractExpiryMails: function (id, adminData, expiryDate) {
    // var agenda = new Agenda({db: {address: config.database}});

    var jobName = 'send contract expiry reminder for societyId ' + id;

    agenda.define(jobName, function (job, done) {
      var adminEmail = job.attrs.data.admin.contactEmail;
      var adminName = job.attrs.data.admin.name;
      var htmlMsg = `<p>Hello ${adminName},</p>`;
      htmlMsg += `<p>Your contract with Society Manager is close to expiry. Please contact Society Manager and renew your plan to receive uninterrupted services of Society Manager.</p>`;
      var mailOpts = {
        from: "noreply@donatemytime.in",
        to: adminEmail,
        subject: `Contract Expiry Reminder!`,
        html: htmlMsg
      }
      smtpTransport.sendMail(mailOpts, function (err, info) {
        if( err) console.log('Mailing error: ', err);
        console.log('mailing.....', info);
      })
      done();
    });

    // agenda.on('ready', function() {
      agenda.cancel({name: jobName}, function(err, numRemoved) {
        if (err) {
          console.log('errrrrr: ', err);
        } else {
          console.log(numRemoved + ' jobs removed............');
          // Add new schedules
          var date1 = moment(expiryDate).add(-2, 'week').toDate();
          var date2 = moment(expiryDate).add(-1, 'week').toDate();
          var date3 = moment(expiryDate).add(-2, 'day').toDate();
          var date4 = moment(expiryDate).add(-1, 'day').toDate();

          // var data = {}
          agenda.schedule(date1, jobName, {admin: adminData});
          agenda.schedule(date2, jobName, {admin: adminData});
          agenda.schedule(date3, jobName, {admin: adminData});
          agenda.schedule(date4, jobName, {admin: adminData});
        }
      });

      // agenda.start(); // moved to app.js
    // });
  }
}
